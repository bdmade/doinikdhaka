# দৈনিক ঢাকা [![CircleCI](https://circleci.com/bb/bdmade/doinikdhaka/tree/master.svg?style=svg)](https://circleci.com/bb/bdmade/doinikdhaka/tree/master)

This project is powered by Ruby on rails.

# Installation Process

Move in college local repository : $ cd doinikdhaka

Install bundler : $ gem install bundler

Install college : $ bundle install

Database creation : $ rake db:create

Database migrate : $ rake db:migrate

Database seed : $ rake db:seed

Run server : $ rails server or $ rails s